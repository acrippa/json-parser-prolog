json_parse(JsonString, JsonObject):-
	core:atom_chars(JsonString, JsonChars),
	phrase(parse_json(JsonObject), JsonChars).

string_parse(JsonString, Term):-
	core:atom_chars(JsonString, JsonChars),
	phrase(parse_string(Term), JsonChars).

parse_json(JsonObject) -->
	parse_object(JsonObject).

parse_json(JsonObject) -->
	parse_array(JsonObject).

parse_object(JsonObject) -->
	['{'],
	object(JsonObject).

object(json_obj(Members)) -->
	parse_members(Members),
	['}'].

object(json_obj([])) -->
	['}'].

parse_members([Pair|Members]) -->
	parse_pair(Pair),
	[','],
	parse_members(Members).

parse_members([Pair]) -->
	parse_pair(Pair).

parse_pair((Key,Value)) -->
	parse_key(Key),
	[':'],
	parse_value(Value).

parse_key(Key) -->
	parse_string(Key).

parse_value(Value) -->
	parse_string(Value).

parse_value(Value) -->
	parse_array(Value).

parse_value(Value) -->
	parse_number(Value).

parse_string(Value) -->
	['"'],
	string(Value).

string(Value) -->
	parse_chars(Value),
	['"'].

parse_chars(Value) -->
	chars(Chars),
	{ core:string_chars(Value, Chars) }.

chars([Char|Chars]) -->
	parse_char(Char),
	chars(Chars).

chars([]) --> [].

parse_char(Char) -->
	[Char],
	{Char \== '"'}.

parse_number(Number) -->
	number(Number).

number(Float) -->
	parse_sign(Chars, CharsInt),
	parse_integer(CharsInt, ['.'|CharsFloat]),
	['.'],
	parse_float(CharsFloat),
	{ core:number_chars(Float, Chars) },
	!.

number(Integer) -->
	parse_sign(Chars, CharsInt),
	parse_integer(CharsInt, []),
	{ core:number_chars(Integer, Chars) }.

parsesign(['+'|T], T) --> ['+'].
parse_sign(['-'|T], T) --> ['-'].
parse_sign(T, T)  --> [].

parse_float(Chars) -->
	parse_digits(Chars, []),
	!.

parse_integer([Digit|Digits], Digits0) -->
    nonzero(Digit),
    !,
    parse_more_digits(Digits, Digits0).

parse_integer([Digit|T], T) -->
    parse_digit(Digit).

nonzero(Digit) -->
    parse_digit(Digit),
    { Digit \== '0' }.

parse_more_digits([Digit|Digits], T) -->
    parse_digit(Digit),
    !,
    parse_more_digits(Digits, T).
parse_more_digits(T, T) --> [].

parse_digits([Digit|Digits], T) -->
    parse_digit(Digit),
    parse_more_digits(Digits, T).

parse_digit(Digit) -->
    [Digit],
    { core:char_type(Digit, digit) }.

parse_array(JsonObject) -->
	['['],
	array(JsonObject).

array(json_array(Elements))-->
	parse_elements(Elements),
	[']'].

array(json_array([]))-->
	[']'].

parse_elements([Element|Elements]) -->
	parse_value(Element),
	[','],
	parse_elements(Elements).

parse_elements([Element]) -->
	parse_value(Element).

json_get(Json, [Key|Keys], Result) :-
	get_json(Json, Key, Result0),
	json_get(Result0, Keys, Result).

json_get(Json, [Key], Result) :-
	get_json(Json, Key, Result).

json_get(Json, Key, Result) :-
	get_json(Json, Key, Result).

get_json(Json, Search, Result) :-
	get_object_value(Json, Search, Result).

get_json(Json, Index, Result) :-
	get_element(Json, Index, Result).

get_object_value(json_obj([Pair|_]), Search, Needle) :-
	get_value(Pair, Search, Needle).

get_object_value(json_obj([_|Tail]), Search, Needle) :-
	get_object_value(json_obj(Tail), Search, Needle).

get_object_value(json_obj([Pair]), Search, Needle) :-
	get_value(Pair, Search, Needle).

get_value((Key, Value), Search, Needle) :-
    Key == Search, 
    Needle = Value.

get_element(json_array(List), Index, Needle) :-
	nth0(Index, List, Needle).