json_parse(JsonString, JsonObject):-
	core:atom_codes(JsonString, JsonChars),
	phrase(parse_json(JsonObject), JsonChars).

string_parse(JsonString, Term):-
	core:atom_codes(JsonString, JsonChars),
	phrase(parse_string(Term), JsonChars).

parse_json(JsonObject) -->
	parse_object(JsonObject).

parse_json(JsonObject) -->
	parse_array(JsonObject).

parse_object(JsonObject) -->
	[123],
	object(JsonObject).

object(json_object(Members)) -->
	parse_members(Members),
	[125].

obejct(json_object([])) -->
	[125].

parse_members([Pair|Members]) -->
	parse_pair(Pair),
	[44],
	parse_members(Members).

parse_members([Pair]) -->
	parse_pair(Pair).

parse_pair((Key,Value)) -->
	parse_key(Key),
	[58],
	parse_value(Value).

parse_key(Key) -->
	parse_string(Key).

parse_value(Value) -->
	parse_string(Value).

parse_string(Value) -->
	[34],
	string(Value).

string(Value) -->
	parse_chars(Value),
	[34].

parse_chars(Value) -->
	chars(Chars),
	{ core:string_codes(Value, Chars) }.

chars([Char|Chars]) -->
	parse_char(Char),
	chars(Chars).
chars([]) --> [].

parse_char(Char) -->
	[Char],
	{Char \== 34}.

parse_array(JsonObject) -->
	[91],
	array(JsonObject).

array(json_array(Elements))-->
	parse_elements(Elements),
	[93].

array(json_array([]))-->
	[93].

parse_elements([Element|Elements]) -->
	parse_value(Element),
	[44],
	parse_elements(Elements).

parse_elements([Element]) -->
	parse_value(Element).