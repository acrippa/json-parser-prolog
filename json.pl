
json_parse(JsonString, JsonObject):-
	core:atom_chars(JsonString, JsonChars),
	phrase(parse_json(JsonObject), JsonChars).

string_parse(JsonString, Term):-
	core:atom_chars(JsonString, JsonChars),
	phrase(parse_string(Term), JsonChars).
    
parse_json(JsonObject) -->
	parse_object(JsonObject).

parse_json(JsonObject) -->
	parse_array(JsonObject).

parse_object(JsonObject) -->
	['{'],
	object(JsonObject).

object(json_object(Members)) -->
	parse_members(Members),
    ['}'].

obejct(json_object([])) -->
	['}'].

parse_members([Pair|Members]) -->
	parse_pair(Pair),
    [','],
    parse_members(Members).

parse_members([Pair]) -->
	parse_pair(Pair).

parse_pair((Key,Value)) -->
	parse_key(Key),
	[':'],
    parse_value(Value).

parse_key(Key) -->
	parse_string(Key).

parse_value(Value) -->
	parse_string(Value).

parse_string(Value) -->
	['"'],
	string(Value).

string(Value) -->
	parse_chars(Value),
	['"'].

parse_chars(Atom) -->
	parse_chars_aux(Chars),
    { core:atom_chars(Atom, Chars) }.

parse_chars_aux([Char|Chars]) -->
    parse_char(Char),
    parse_chars_aux(Chars).
parse_chars_aux([]) --> [].

parse_char(Char) -->
    [Char],
    {Char \== '"'}.

parse_array(JsonObject) -->
	['['],
	array(JsonObject).

array(json_array(Elements))-->
	parse_elements(Elements),
	[']'].

array(json_array([]))-->
	[']'].

parse_elements([Element|Elements]) -->
	parse_value(Element),
	[','],
	parse_elements(Elements).

parse_elements([Element]) -->
	parse_value(Element).